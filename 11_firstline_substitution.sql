﻿do $$
declare r record;
declare intid int;
begin

for r in
	Select distinct d.patientid,d.uniquepatientnumber, a.substitution_date datestarted, e.regimenid regimen
	, case trim(a.regother) when '' then null else trim(regother) end regother
	, f.decodeid artchangereason
	 from public.tblsubstitutecodesreg a left join public.tlkswitchreasons b on a.reasons = b.switchcode
	inner join public.tlkregimenfirst c on a.regimen = c.regnum
	inner join cpad.vw_patient d on a.patient_id::text = d.uniquepatientnumber
	left join cpad.mst_regimen e on lower(firstregimen) = e.regimen
	left join cpad.vw_codedecode f on lower(b.switchreason) = f.decodename and f.codename = 'art change reasons'
	where a.firstor2ndlinesub = 1
loop 
	intid:= pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdateartsubstitution(r.patientid::text, intid::text, '1'::text, r.datestarted::text, r.regimen::text, r.regother::text, r.artchangereason::text,'true'::text);	
end loop;
end$$;