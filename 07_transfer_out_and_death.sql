﻿do $$
declare r record;
declare intid int;
begin

for r in

	select distinct
	c.patientid
	, d.decodeid careendreason
	, case trim(a.transfer_location) when '' then null else trim(a.transfer_location) end facilitytransferredto
	, coalesce(a.fup_status_date, e.lastvisit) careenddate
	from "public".tbllost_to_followup a inner join "public".tlktranscode b on a.fup_status = b.transcode
	inner join cpad.vw_patient c on a.patient_id::text = c.uniquepatientnumber
	inner join cpad.vw_codedecode d on case lower(b.transname) 
		when 'died' then 'died'
		when 'transfer out' then 'transferred out'
	else lower(b.transname) end = d.decodename and d.codename = 'art care end reasons'
	inner join (select patient_id, max(visit_date) lastvisit from public.tblvisit_information group by patient_id) e on a.patient_id = e.patient_id

loop 
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdatetreatmentinterruptions(r.patientid::text, intid::text, r.careenddate::text, r.careendreason::text, null::text, r.facilitytransferredto::text);
end loop;
end$$;