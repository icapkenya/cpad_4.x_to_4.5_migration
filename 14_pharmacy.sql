﻿do $$
declare rec record;
declare intid int;
declare i numeric := 0;
declare n numeric:= count(visit_id)visits from public.tblvisit_information; --41016 vs 40954
begin

for rec in

select distinct
b.patientid
, a.visit_id
, a.visit_date visitdate
-- , c.decodeid visittype
-- , d.decodeid encountertype
, z.userid providerid
-- , z.userid 
-- , a.next_visit_date appointmentdate
, k.decodeid ctxadherence
, a.cotrim ctxdispensed
, case a."Dapson" when '1' then 1 else null end as dapsonedispensed
, case lower(a."INH") when 'y' then 1 else null end as inhdispensed
, case trim(a.other_medication) when '' then null else trim(a.other_medication) end othermedication
, o.decodeid artadherence
, m.regimenid artregimen
, q.decodeid adherencereasons
, case when a.art_regimen is not null then datediff('dd',a.visit_date,a.next_visit_date) else null end duration
 from public.tblvisit_information a inner join vw_patient b on a.patient_id::text = b.uniquepatientnumber 
 left join cpad.vw_codedecode c on case "VisitType" when 1 then 'scheduled' when 2 then 'unscheduled' else 'scheduled' end = c.decodename and c.codename = 'visit types'
 left join cpad.vw_codedecode d on case sf when 1 then 'sf - self' when 2 then 'ts - treatment supporter' else 'sf - self' end = d.decodename and d.codename = 'encounter types'
 left join public.tlkadherencestatus j on a.cotrim_adherence = j.adherecode
 left join cpad.vw_codedecode k on lower(j.adherence) = k.decodename and k.codename = 'ctx adherence status'
 left join public.tlkregimenfirst l on a.art_regimen = l.regnum
 left join cpad.mst_regimen m on lower(l.firstregimen) = m.regimen
 left join public.tlkadherencestatus n on a.art_adherence = n.adherecode
 left join cpad.vw_codedecode o on lower(n.adherence) = o.decodename and o.codename = 'art adherence status' 
 left join public.tlkadherencev p on a.art_adherence_reason = p.id
 left join cpad.vw_codedecode q on lower(p.name) = q.decodename 
,(select userid from mst_user where username = 'admin') z
where coalesce( k.decodeid::text
, a.cotrim::text 
, case a."Dapson" when '1' then 1::text else null::text end 
, case lower(a."INH") when 'y' then 1::text else null::text end  
, case trim(a.other_medication) when '' then null::text else trim(a.other_medication)::text end
, o.decodeid::text
, m.regimenid::text) is not null 
order by a.visit_date 

loop
RAISE NOTICE '% ,% percent done',rec.visit_id,100*(i/n);
-- intid := pr_saveupdatevisitdetails from pr_saveupdatevisitdetails(rec.patientid::text,rec.visitdate::text, rec.visittype::text
-- ,rec.encountertype::text, rec.appointmentdate::text, rec.providerid::text, rec.userid::text, null::text);
intid := interactionid from cpad.ord_interaction where patientid = rec.patientid and interactiondate = rec.visitdate::date 
and interactiontypeid = (select decodeid from cpad.mst_decode where decodename = 'moh 257 visit')
limit 1;
if(intid > 0) then
perform * from cpad.pr_saveupdatepharmacy(intid::text, rec.ctxadherence::text ,rec.ctxdispensed::text,rec.dapsonedispensed::text,rec.inhdispensed::text
		,rec.othermedication::text,rec.artadherence::text,rec.adherencereasons::text,rec.artregimen::text,rec.duration::text,rec.providerid::text, rec.visitdate::text);
end if;
i := i + 1;
end loop;

end$$;