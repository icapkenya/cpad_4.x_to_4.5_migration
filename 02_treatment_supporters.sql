﻿do $$
declare r record;
declare pid int;
declare intid int;

begin

for r in

select distinct d.patientid
, a.first_name firstname
, a.last_name lastname
, c.decodeid relationshipid
, a.relationship_other otherrelationship
, a.postal_address postaladdress
, a.telephone  from public.tbltreatment_supporter a
left join public.tlksupporter_relationships b on a.relationship = relationid
left join cpad.vw_codedecode c on case when lower(b.relationship) is null then 'other (specify)' else lower(b.relationship) end = c.decodename and c.codename = 'relationship types'
inner join cpad.vw_patient d on a.patient_id::text = d.uniquepatientnumber::text


loop	
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from pr_saveupdatepatientcontact(r.patientid::text,intid::text,r.firstname::text,r.lastname::text
	,r.relationshipid::text,r.otherrelationship::text,r.postaladdress::text,r.telephone::text,'treatmentsupporter'::text
	,''::text,''::text,''::text,''::text);

end loop;
end$$;