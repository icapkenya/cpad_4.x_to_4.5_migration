﻿do $$
declare rec record;
declare intid int;
declare i numeric := 0;
declare n numeric:= count(visit_id)visits from public.tblvisit_information; --41016 vs 40954
begin

for rec in

select distinct
b.patientid
, a.visit_id
, a.visit_date visitdate
, c.decodeid visittype
, d.decodeid encountertype
, z.userid providerid
, z.userid 
, a.next_visit_date appointmentdate
---------------------
, a.height
, a.weight
, a."BPS" bps
, a."BPD" bpd
---------------------
, e.decodeid pregnancystatus
, h.decodeid notpregnantreason
, delivery_date edd
, g.decodeid whostage
-----------------------
, i.decodeid tbstatus
, a."TBStDate" tbstartdate
, a."TBEdDate" tbenddate
, a."tb_Tx" tbnumber

, r.decodeid atriskpop
, a."PwPDis" pwpdisclosure
, a."PwPPaT" pwppartnertested
, a."PwPCon" pwpcondoms
, a."PwPSTI" pwpstiscreening

from public.tblvisit_information a inner join vw_patient b on a.patient_id::text = b.uniquepatientnumber 
left join vw_codedecode c on case "VisitType" when 1 then 'scheduled' when 2 then 'unscheduled' else 'scheduled' end = c.decodename and c.codename = 'visit types'
left join vw_codedecode d on case sf when 1 then 'sf - self' when 2 then 'ts - treatment supporter' else 'sf - self' end = d.decodename and d.codename = 'encounter types'
left join vw_codedecode e on case a.pregnancy when 1 then 'yes' when 2 then 'no' else null end = e.decodename and e.codename = 'pregnancy status'
left join public.tlkwhostage f on a."WHOstage" = f.whocode
left join vw_codedecode g on lower(f.whostage) = g.decodename and g.codename = 'who stage'
left join vw_codedecode h on case a.no_pg when 1 then 'ab - (induced abortion)' when 2 then 'mc - (miscarriage)' else null end = h.decodename and h.codename = 'not pregnant reasons'
left join vw_codedecode i on case a.tb_status when 1 then 'no signs' when 2 then 'tb suspected' when 3 then 'tb rx' when 4 then 'not done' else null end = i.decodename and i.codename = 'tb status'
left join cpad.vw_codedecode r on lower(a."RiskPopu") = r.decodename and r.codename = 'at risk populations'
 
,(select userid from mst_user where username = 'admin') z

order by a.visit_date 

loop
RAISE NOTICE '% ,% percent done',rec.visit_id,100*(i/n);
intid := pr_saveupdatevisitdetails from cpad.pr_saveupdatevisitdetails(rec.patientid::text,rec.visitdate::text, rec.visittype::text
,rec.encountertype::text, rec.appointmentdate::text, rec.providerid::text, rec.userid::text, null::text);

perform * from cpad.pr_saveupdatevitals(intid::text,rec.weight::text,rec.height::text,rec.bps::text,rec.bpd::text);
perform * from cpad.pr_saveupdateclinicalstatus(intid::text, rec.pregnancystatus::text, rec.notpregnantreason::text, rec.edd::text, rec.whostage::text);
perform * from cpad.pr_saveupdatetbstatus(intid::text,rec.tbstatus::text, rec.tbnumber::text, rec.tbstartdate::text, rec.tbenddate::text);
perform * from cpad.pr_saveupdatepwpservices(intid::text, rec.atriskpop::text, rec.pwpdisclosure::text, rec.pwppartnertested::text
, rec.pwpcondoms::text, rec.pwpstiscreening::text);
i := i + 1;
end loop;

end$$;