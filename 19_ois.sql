﻿DO $$
Declare r record;
declare ptsid int;

BEGIN
For r in

Select 
distinct a.interactionid
 , a.interactiondate 
 , a.patientid
 , e.decodeid oi
FROM 
cpad.ord_interaction a left join cpad.dtl_patientidentifier b on a.patientid = b.patientid
left join "public"."tblNewOI" c on b.identifiervalue = c.patient_id::text and a.interactiondate = c.visit_date 
left join "public".tlkoi_code d on c.newoi = d.oi_id
left join cpad.vw_codedecode e 
on case 
when d.oi_name in ('Z - Zoster','1-Zoster') then 'herpes zoster'
when d.oi_name in ('Thrush - Thrush (oral/vaginal)' , '4-Thrush (oral/vaginal)') then 'oral candidiasis thrush'
when d.oi_name in ('U - Ulcers (mouth, etc)' , '5-Ulcers (mouth, etc)') then 'recurrent oral ulceration'
when d.oi_name in ('DE / Enceph - DEmentia/Enceph' , '3-DEmentia/Enceph') then 'encephalopathy dementia'
when d.oi_name in ('W - Weight loss' , '11-Weight loss') then 'weight loss > 10%'
when d.oi_name in ('P - Pneumonia' , '2-Pneumonia') then 'pneumonia' else null end = e.decodename 
where e.decodename is not null 

LOOP 
IF(r.oi > 0) THEN
INSERT INTO cpad.dtl_patientinfection (interactionid, infectionid, infectionstartdate
, infectionenddate, iscurrent, infectiontext, createdate, updatedate, deleteflag) VALUES
(r.interactionid, r.oi, null, null, true, null, now()::date, null, false);
End IF;

END LOOP;
END$$;