﻿do $$
declare r record;
declare pid int;
declare intid int;

begin

for r in

Select a.patient_id
, max(lower(a.first_name)) firstname
, max(lower(a.last_name)) lastname
, max(c.decodeid) gender
, max(a.dob) dob
, max(coalesce(a.hiv_care_date,e.fvisit)) registrationdate
, max(a.patient_id) uniquepatientnumber
, max(w.telephone) telephone
, max(w.district) subcounty
, max(w.sub_location) sublocation
, max(w.location) "location"
, max(w.nearest_health_center) nearesthealthcentre
, max(w.nearest_school) landmark
, max(j.decodeid) maritalstatus
, max(w.postal_address) postaladdress
from "public".tblpatient_information a left join "cpad".mst_decode c on case when a.sex = 1 then 'male' else 'female' end = c.decodename
left join 
(select patient_id, min(visit_date) fvisit 
from "public".tblvisit_information
group by patient_id) e on a.patient_id = e.patient_id
left join "public".tbladdress w on a.patient_id = w.patient_id
left join "public".tlkmarital x on a.marital_status = x.maritalcode 
left join cpad.vw_codedecode j on x.maritalname = j.decodename and j.codename = 'marital status'
where coalesce(a.hiv_care_date,e.fvisit) is not null
group by
a.patient_id
order by registrationdate

loop

	pid := pr_saveupdatepatient from cpad.pr_saveupdatepatient('true'::text,r.firstname::text, r.lastname::text, r.gender::text, r.dob::text, r.registrationdate::text, ''::text);
	intid := pr_saveupdatepatientidentifiers from cpad.pr_saveupdatepatientidentifiers('true'::text,pid::text,r.uniquepatientnumber::text, '');
	perform * from cpad.pr_saveupdatepatientinformation('true'::text,intid::text,r.telephone::text,r.subcounty::text ,r.sublocation::text,r.location::text,r.nearesthealthcentre::text
	,r.landmark::text,r.maritalstatus::text, r.postaladdress::text, null::text);

end loop;
end$$;