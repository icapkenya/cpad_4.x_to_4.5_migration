﻿do $$
declare r record;
declare intid int;
begin

for r in

select 
b.patientid
, ARRAY[['eligiblewho',i.decodeid::text],['eligiblecd4count',a."CD4_Count"::text],
['eligiblecd4percent',a."CD4_percent"::text],
['eligiblelt10',a.eligibility_child::text],
['eligiblepregnant',a.eligibility_pregnant::text],
['eligiblediscordant',a.eligibility_discordant::text],
['eligiblelactating',a.eligibility_lactating::text],
['eligibilehepb',a.eligibility_hepb::text]
] criteria
from  "public".tblpatient_information a 
inner join cpad.vw_patient b on a.patient_id::text = b.uniquepatientnumber
left join public.tlkwhostage h on a."clinical_who_stage" = h.whocode
left join vw_codedecode i on lower(h.whostage) = i.decodename 
and i.codename = 'art eligibility reasons'
where coalesce("clinical_who_stage"::text,
"CD4_Count"::text,
"CD4_percent"::text,
"eligibility_child"::text ,
"eligibility_pregnant"::text ,
"eligibility_discordant"::text ,
"eligibility_lactating"::text ,
"eligibility_hepb"::text) is not null 
and a.art_eligibility_date is not null

loop 
	intid:= pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	RAISE NOTICE 'intid = %',intid;
	perform * from cpad.pr_saveupdateeligbilitycriteria(r.patientid::text, intid::text, r.criteria);
end loop;
end$$;