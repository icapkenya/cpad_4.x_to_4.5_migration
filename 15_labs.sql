﻿do $$
declare rec record;
declare intid int;
declare i numeric := 0;
declare n numeric:= count(visit_id)visits from public.tblvisit_information; --41016 vs 40954
begin

for rec in

select distinct
b.patientid
, a.visit_id
, a.visit_date visitdate
, c.decodeid visittype
, d.decodeid encountertype
, z.userid providerid
, z.userid 
, a.next_visit_date appointmentdate
, a.cd4date
, a.cd4_result cd4count
, a.cd4_results_percent cd4percent
, a.hb_result hb
, a."RPR_result" rpr
, a."TBSputum_result" tbsputum
, a."ViralLoad_result" viralload
 from public.tblvisit_information a inner join vw_patient b on a.patient_id::text = b.uniquepatientnumber 
 left join cpad.vw_codedecode c on case "VisitType" when 1 then 'scheduled' when 2 then 'unscheduled' else 'scheduled' end = c.decodename and c.codename = 'visit types'
 left join cpad.vw_codedecode d on case sf when 1 then 'sf - self' when 2 then 'ts - treatment supporter' else 'sf - self' end = d.decodename 
 and d.codename = 'encounter types' left join cpad.vw_codedecode r on lower(a."RiskPopu") = r.decodename and r.codename = 'at risk populations'
 ,(select userid from mst_user where username = 'admin') z
where coalesce(
a.cd4date::text
, a.cd4_result::text
, a.cd4_results_percent::text
, a.hb_result::text
, a."RPR_result"::text
, a."TBSputum_result"::text
, a."ViralLoad_result"::text) is not null 
order by a.visit_date 

loop
RAISE NOTICE '% ,% percent done',rec.visit_id,100*(i/n);
-- intid := pr_saveupdatevisitdetails from pr_saveupdatevisitdetails(rec.patientid::text,rec.visitdate::text, rec.visittype::text
-- ,rec.encountertype::text, rec.appointmentdate::text, rec.providerid::text, rec.userid::text, null::text);

intid := interactionid from cpad.ord_interaction where patientid = rec.patientid and interactiondate = rec.visitdate::date 
and interactiontypeid = (select decodeid from mst_decode where decodename = 'moh 257 visit')
limit 1;
if(intid > 0) then
perform * from cpad.pr_saveupdatelabs(intid::text, rec.cd4date::text, rec.cd4count::text, rec.cd4percent::text, rec.hb::text
		, rec.rpr::text, rec.tbsputum::text, rec.viralload::text, rec.providerid::text, rec.visitdate::text);
end if;
i := i + 1;
end loop;

end$$;



