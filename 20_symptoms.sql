﻿insert into cpad.dtl_patientsymptom (interactionid, symptomid, symptomstartdate, symptomenddate, symptomtext
, createdate, updatedate, deleteflag)
Select distinct
a.interactionid
, e.decodeid symptomid
, null::date
, null::date 
, case trim(c.newoiother) when '' then null else trim(c.newoiother) end symptomtext
, now()::date
, null::date 
, false
FROM
cpad.ord_interaction a inner join cpad.dtl_patientidentifier b
on a.patientid = b.patientid
left join "public"."tblNewOI" c on b.identifiervalue = c.patient_id::text
and a.interactiondate = c.visit_date 
left join "public".tlkoi_code d on c.newoi = d.oi_id
left join cpad.vw_codedecode e 
on case 
when d.oi_name in ('6-Fever') then 'fever'
when d.oi_name in ('7-Cough') then 'cough'
when d.oi_name in ('8-DB difficult breathing') then 'difficult breathing'
when d.oi_name in ('12-UD urethral discharge') then 'urethral discharge'
when d.oi_name in ('Other Medical Conditions (please specify)') then 'other condition (please specify)'
else null end = e.decodename 
where e.decodename is not null;