﻿insert into cpad.dtl_patientsymptom(interactionid, symptomid, symptomtext, createdate, deleteflag)

select c.interactionid
, e.decodeid symptomid
, case trim(othersideeffects) when '' then null
else trim(othersideeffects) end as symptomtext
, now()::date
, false
from public."tblARTSideEffects" a
inner join cpad.dtl_patientidentifier b on a.patient_id::text = b.identifiervalue
inner join cpad.ord_interaction c on b.patientid = c.patientid and a.visit_date = c.interactiondate
inner join public.tlkartsideeffects d on a.artsideeffects = d.sideeffectscode
inner join cpad.vw_codedecode e on lower(d.artsideeffects) = e.decodename and e.codename = 'art side effects'