﻿do $$
declare rec record;
declare intid int;
declare i numeric := 0;
declare n numeric:= count(a.visit_id)visits from public.tblvisit_information a where coalesce(a.referred_to::text, a.days_hospitalized::text) is not null; 
begin

for rec in

select distinct
b.patientid
, a.visit_id
, a.visit_date visitdate
, c.decodeid visittype
, d.decodeid encountertype
, z.userid providerid
, z.userid 
, a.next_visit_date appointmentdate
, a.referred_to referredto
, a.days_hospitalized dayshospitalized

from public.tblvisit_information a inner join vw_patient b on a.patient_id::text = b.uniquepatientnumber 
left join cpad.vw_codedecode c on case "VisitType" when 1 then 'scheduled' when 2 then 'unscheduled' else 'scheduled' end = c.decodename and c.codename = 'visit types'
left join cpad.vw_codedecode d on case sf when 1 then 'sf - self' when 2 then 'ts - treatment supporter' else 'sf - self' end = d.decodename and d.codename = 'encounter types'
,(select userid from cpad.mst_user where username = 'admin') z
where coalesce(a.referred_to::text
, a.days_hospitalized::text) is not null
order by a.visit_date

loop
RAISE NOTICE '% ,% percent done',rec.visit_id,100*(i/n);
-- intid := cpad.pr_saveupdatevisitdetails from cpad.pr_saveupdatevisitdetails(rec.patientid::text,rec.visitdate::text, rec.visittype::text
-- ,rec.encountertype::text, rec.appointmentdate::text, rec.providerid::text, rec.userid::text, null::text);

intid := interactionid from cpad.ord_interaction where patientid = rec.patientid and interactiondate = rec.visitdate::date 
and interactiontypeid = (select decodeid from cpad.mst_decode where decodename = 'moh 257 visit')
limit 1;
if(intid > 0) then
perform * from cpad.pr_saveupdatereferrals(intid::text, rec.referredto::text, rec.dayshospitalized::text);
end if;
i := i + 1;
end loop;

end$$;