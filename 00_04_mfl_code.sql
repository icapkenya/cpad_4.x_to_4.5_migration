\prompt 'Enter MFL:' mflcode
UPDATE cpad.mst_facility SET configured = false;
UPDATE cpad.mst_facility SET configured = true WHERE mflcode = ::mflcode;
SELECT CONCAT('Configured facility is: ', facilityname, ' MFL:', mflcode) AS message FROM cpad.mst_facility WHERE configured = true LIMIT 1;