﻿do $$
declare r record;
declare pid int;
declare intid int;
declare contactpatientid text;

begin

for r in

select distinct d.patientid
, a."FmailyMemName" firstname
, a."FmailyMemAge" contactage
, c.decodeid relationshipid
, e.decodeid hivstatus
, g.decodeid hivcarestatus
, case trim(a."FmailyMemCCCN"::text) when '' then null when '0' then null else trim(a."FmailyMemCCCN"::text) end as  p

 from public.tblfamilymembers a 
left join public.tlksupporter_relationships b on a."FmailyMemRel" = b.relationid
left join cpad.vw_codedecode c on case when lower(b.relationship) is null then 'other (specify)' else lower(b.relationship) end = c.decodename and c.codename = 'relationship types'
left join cpad.vw_codedecode e on case a."FmailyMemHIV" when 1 then 'positive' when 2 then 'negative' else 'unknown' end = e.decodename and e.codename = 'hiv status'
left join public.tlkyesno f on a."FmailyMemCare" = f.yesnocode
left join cpad.vw_codedecode g on case lower(f.yesno) when 'yes' then 'on care' when 'no' then 'not on care' else 'unknown' end = g.decodename and g.codename = 'hiv care status'
inner join cpad.vw_patient d on a.patient_id::text = d.uniquepatientnumber::text


loop	
contactpatientid := patientid::text from cpad.vw_patient where uniquepatientnumber = r.p;
if(contactpatientid = null) then contactpatientid := ''; end if;
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from pr_saveupdatepatientcontact(r.patientid::text,intid::text,r.firstname::text,''::text
	,r.relationshipid::text,''::text,''::text,''::text,'familymember'::text
	,r.hivstatus::text,r.hivcarestatus::text,r.contactage::text,contactpatientid);

end loop;
end$$;
