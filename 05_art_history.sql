﻿do $$
declare r record;
declare intid int;

begin

for r in

select distinct b.patientid
, case when previous_arv = 1 then f.decodeid 
when previous_arv = 2 then g.decodeid
else null end as previousarv
, hiv_pos_date datehivpositive
, hiv_pos_place locationhivpositive
, hiv_care_date dateenrolled
, x.decodeid enrollmentwho
, pep_date datepepoffered
, case z.pepname when 'sexual assault' then i.decodeid
when 'occupational exposure' then j.decodeid
when 'others (specify)' then k.decodeid else null end as pepreason
, pep_reason_other otherpepreason

from "public".tblpatient_information a
inner join cpad.vw_patient b on a.patient_id::text = b.uniquepatientnumber
left join "public".tlkpepreason z on a.pep_reason = z.pepcode
left join public.tlkwhostage y on a.who_stage = y.whocode
left join vw_codedecode x on lower(y.whostage) = x.decodename and x.codename = 'enrollment who stage'
,(select decodeid, decodename from cpad.vw_codedecode where codename = 'previously on arvs' and decodename = 'yes') f
,(select decodeid, decodename from cpad.vw_codedecode where codename = 'previously on arvs' and decodename = 'no') g
,(select decodeid, decodename from cpad.vw_codedecode where codename = 'pep treatment reasons' and decodename = 'sexual assault') i
,(select decodeid, decodename from cpad.vw_codedecode where codename = 'pep treatment reasons' and decodename = 'occupational exposure') j
,(select decodeid, decodename from cpad.vw_codedecode where codename = 'pep treatment reasons' and decodename = 'other reason') k

loop
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdatearthistory(r.patientid::text,intid::text, r.previousarv::text, r.datehivpositive::text, r.locationhivpositive::text, r.dateenrolled::text
				, r.enrollmentwho::text, r.datepepoffered::text, r.pepreason::text, r.otherpepreason::text);
end loop;
end$$;

