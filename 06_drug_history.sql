﻿do $$
declare r record;
declare intid int;
begin

for r in
	select distinct b.patientid
			, l.decodeid drugpurpose
			, e.regimenid regimen
			, case when trim(a.arthistorydrugoth) = '' then null else trim(a.arthistorydrugoth) end otherdrug
			, a.arthistorydate datelastused 
			from "public"."tblARTdrughistory" a
			inner join cpad.vw_patient b on a.patient_id::text = b.uniquepatientnumber
			left join "public".tlkregimenhistory c on a.arthistorydrug = c.code
			left join "public".tlkregimenfirst d on a.arthistorydrug = d.regnum
			left join cpad.mst_regimen e on lower(coalesce(c.regimenhistory, d.firstregimen)) = e.regimen 
			,(select decodeid, decodename from cpad.vw_codedecode where codename = 'treatment types' and decodename = 'art') l
			where coalesce(e.regimenid::text 
			, case when trim(a.arthistorydrugoth) = '' then null else trim(a.arthistorydrugoth) end 
			, a.arthistorydate::text) is not null  
			
		loop
			intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
			perform * from cpad.pr_saveupdatedrughistory(r.patientid::text, intid::text, r.drugpurpose::text, r.regimen::text, r.otherdrug::text, r.datelastused::text);
		end loop;
end$$;