﻿do $$
declare r record;
declare intid int;
begin

for r in

	select c.patientid,c.uniquepatientnumber,
	a.art_interruption_date careenddate
	,d.decodeid careendreason
	,a.date_restarted restartdate  
	from public."tblARTInterruptions" a inner join public.tlktreatmentinterruption
	b on a.interruptions_reason = b.interruptionscode
	inner join cpad.vw_patient c on a.patient_id::text = c.uniquepatientnumber
	left join cpad.vw_codedecode d on lower(b.interruptions) = d.decodename and d.codename = 'art treatment interruption reasons'

loop 
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdatetreatmentinterruptions(r.patientid::text, intid::text, r.careenddate::text, r.careendreason::text, r.restartdate::text
	, null::text);
end loop;
end$$;