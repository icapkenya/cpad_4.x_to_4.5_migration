﻿INSERT INTO "cpad".dtl_patientfamilyplanning(interactionid, familyplanningstatusid, familyplanningstatusreasonid
				, familyplanningmethodid, familyplanningmethodtext, createdate, updatedate, deleteflag)

select distinct a.interactionid 
, g.decodeid fpstatusid
, null::int fpreasonid
, e.decodeid fpmethodid 
, case c.fpother when '' then null else c.fpother end fpmethodtext
, now()::date cd
, null::date upd
, false df
from cpad.ord_interaction a 
inner join cpad.vw_patient b on a.patientid = b.patientid
left join "public".tblfpmethod c on b.uniquepatientnumber = c.patient_id::text and a.interactiondate::date = c.visit_date::date 
left join "public".tlkfpmethod d on c.fpmethod = d.fpmethodcode
left join cpad.vw_codedecode e on lower(d.fpmethod) = e.decodename
left join "public".tblvisit_information f on b.uniquepatientnumber = f.patient_id::text and a.interactiondate::date = f.visit_date::date
left join cpad.vw_codedecode g on case f.fp_status when 1 then 'currently on family planning'
when 2 then 'not on family planning'
when 3 then 'wants family planning' else null end = g.decodename 
where f.fp_status is not null order by a.interactionid;