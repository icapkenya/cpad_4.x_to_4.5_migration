﻿do $$
declare r record;
declare intid int;
begin

for r in

select distinct
b.patientid,
"art_eligibility_date" dateeligible,
coalesce(a.artstart_date ,"date_1stline_regimen") artstartdate,
g.regimenid startregimen,
case trim(regimen_other) when '' then null else trim(regimen_other) end regimentext,
"art_start_wgt" bweight,
"art_start_hgt" bheight,
i.decodeid bwho
from "public".tblpatient_information a 
inner join cpad.vw_patient b on a.patient_id::text = b.uniquepatientnumber
left join "public".tlkregimenfirst f on a.regimen_started = f.regnum
left join cpad.mst_regimen g on lower(f.firstregimen) = g.regimen 
left join public.tlkwhostage h on a.art_start_who = h.whocode
left join vw_codedecode i on lower(h.whostage) = i.decodename 
and i.codename = 'art baseline characteristics'
where coalesce(a.art_eligibility_date::text, date_1stline_regimen::text,a.artstart_date::text) is not null

loop 
	intid:= pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdateartstart(r.patientid::text, intid::text, r.dateeligible::text
	, r.artstartdate::text, r.startregimen::text, r.regimentext::text, r.bweight::text
	, r.bheight::text, null::text, r.bwho::text);
end loop;
end$$;