﻿do $$
declare r record;
declare pid int;
declare intid int;
declare contactpatientid text;

begin

for r in

select distinct b.patientid
, e.decodeid patientsource
, case trim(a.source_other) when '' then null else trim(a.source_other) end otherentrypoint
, a.transfer_date transferindate
, a.transfer_from district
, a.transfer_facility transferinfrom
, a.artstart_date artstartdate
from "public"."tblpatient_information" a
inner join cpad.vw_patient b on a.patient_id::text = b.uniquepatientnumber
left join "public".tlkpatientsource d on a.patient_source = d.sourcecode
left join cpad.vw_codedecode e on lower(d.sourcename) = e.decodename and e.codename = 'service entry points'

where coalesce (e.decodeid::text 
, case trim(a.source_other) when '' then null else trim(a.source_other) end
, a.transfer_date::text 
, a.transfer_from::text 
, a.transfer_facility::text 
, a.artstart_date::text ) is not null

loop	
	intid := pr_saveupdatepatientprofile from cpad.pr_saveupdatepatientprofile(r.patientid::text);
	perform * from cpad.pr_saveupdatepatientsource(r.patientid::text, intid::text, r.patientsource::text, r.otherentrypoint::text, r.transferindate::text
	, r.district::text, r.transferinfrom::text, r.artstartdate::text);
end loop;
end$$;